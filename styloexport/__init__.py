import logging
from typing import Optional

from flask import Flask

from . import api, commands, views

__version__ = "2.4.1"

__pdoc__ = {}
__pdoc__["create_app"] = False
__pdoc__["article"] = False
__pdoc__["commands"] = False
__pdoc__["config"] = False
__pdoc__["forms"] = False
__pdoc__["pandocapi"] = False
__pdoc__["styloapi"] = False
__pdoc__["utils"] = False
__pdoc__["views"] = False
__pdoc__["wsgi"] = False


logger = logging.getLogger("styloexport")
logger.setLevel(logging.DEBUG)
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("[%(asctime)s] %(levelname)s in %(module)s: %(message)s")
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


def create_app(test_config: Optional[dict] = None) -> Flask:
    # create and configure the app
    app = Flask(__name__)

    app.config.from_pyfile("config.py")
    if test_config:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    app.config["MAX_CONTENT_LENGTH"] = (
        app.config["SE_MAX_CONTENT_LENGTH_MB"] * 1000 * 1000
    )
    app.config["MAX_FORM_MEMORY_SIZE"] = (
        app.config["SE_MAX_FORM_MEMORY_SIZE_MB"] * 1000 * 1000
    )
    app.register_blueprint(views.blueprint)
    app.register_blueprint(api.blueprint, url_prefix="/api")
    app.register_blueprint(commands.blueprint)
    return app


app = create_app()
