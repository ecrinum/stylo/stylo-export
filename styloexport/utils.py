import json
import os
from operator import attrgetter
from pathlib import Path
from typing import Any, Iterator, List, Tuple, Union
from urllib.parse import urlparse

from dotenv import load_dotenv

from .config import *  # noqa

load_dotenv()


def get_domain_from_url(url: str) -> str:
    return urlparse(url).netloc


def get_title_from_csl(csl_path: Path) -> str:
    """Return the title of a CSL without an XML parser (a bit fragile)."""
    _, title_remaining = csl_path.read_text().split("<title>", 1)
    title, remaining = title_remaining.split("</title>", 1)
    return title


def get_bibliographic_styles() -> dict[str, List[Tuple[str, str, dict]]]:
    """Return template-related (first) + Zotero pre-selected styles."""
    template_related: List[Tuple[str, str, dict]] = []
    templates_folder = Path() / "templates"
    for folder in each_folder_from(templates_folder, exclude="templates"):
        for csl_file_path in sorted(Path(folder).glob("*.csl")):
            name = csl_file_path.stem
            title = get_title_from_csl(csl_file_path)
            template_related.append(
                (
                    name,
                    title,
                    {
                        "title": title,
                        "name": name,
                        "dependent": 1,
                        "categories": {"format": "", "fields": ["humanities"]},
                        "updated": "2023-12-02 00:00:00",
                        "href": f"https://www.zotero.org/styles/{name}",
                    },
                )
            )
    zotero_selected = [
        (style["name"], style["title"], style)
        for style in (
            json.loads(
                (get_env_var("SE_STYLES_DIR") / "styles-custom.json").read_text()
            )
        )
    ]

    def chicago_modified_first(item: Tuple[str, str, dict]) -> bool:
        return item[0] != "chicagomodified"

    return {
        "Styles personnalisés": sorted(template_related, key=chicago_modified_first),
        "Styles Zotero": zotero_selected,
    }


def get_env_var(name: str, default: Any = None) -> Any:
    """Get the env var from a .env file, fallback on config.py."""
    env_var = os.getenv(name)
    if env_var is None:
        try:
            # This is quite ugly.
            env_var = globals()[name]
        except KeyError as e:  # pragma: no cover
            print(f"Env var `{name}` not found.")
            if default is not None:
                return default
            else:
                raise e
    return env_var


def each_folder_from(
    source_dir: Path, exclude: Union[str, None] = None
) -> Iterator[os.DirEntry]:
    """Walk across the `source_dir` and return the folder paths."""
    for direntry in sorted(os.scandir(source_dir), key=attrgetter("name")):
        if direntry.is_dir():
            if exclude is not None and direntry.name in exclude:
                continue
            yield direntry


def strtobool(val: str) -> int:
    """Convert a string representation of truth to true (1) or false (0).
    True values are 'y', 'yes', 't', 'true', 'on', and '1'; false values
    are 'n', 'no', 'f', 'false', 'off', and '0'.  Raises ValueError if
    'val' is anything else.
    """
    if isinstance(val, bool):
        return 1 if val else 0
    val = val.lower()
    if val in ("y", "yes", "t", "true", "on", "1"):
        return 1
    elif val in ("n", "no", "f", "false", "off", "0"):
        return 0
    else:
        raise ValueError(f"Invalid truth value {val}")
