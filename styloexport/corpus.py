import logging
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional
from zipfile import ZipFile

import httpx
import wrapt
import yaml
from slugify import slugify

from .article import Article
from .styloapi import StyloAPI
from .utils import get_domain_from_url

logger = logging.getLogger("styloexport")


@wrapt.decorator
def bail_on_errors(wrapped: Callable, instance: Any, args: list, kwargs: dict) -> Any:
    """Decorator to stop processing an article which contains errors."""
    if instance.errors:
        return
    return wrapped(*args, **kwargs)


class Corpus:
    def __init__(
        self,
        edition: str,
        edition_configuration: dict,
        domain: str,
        id: str,
        slug: str,
        title: str,
        download_dir: Path,
        allowed_editions: List[str],
        allowed_base_urls: List[str],
        supported_images_extensions: List[str],
    ) -> None:
        self.edition = edition
        self.edition_configuration = edition_configuration
        self.domain = domain
        self.domain_slug = slugify(domain)
        self.id = id
        self.slug = slug
        self.name = title
        self.root_download_dir = download_dir
        self.download_dir = (
            download_dir / edition / self.domain_slug / "corpus" / f"{slug}-{id}"
        )
        self.download_dir.mkdir(parents=True, exist_ok=True)
        self.errors: List[str] = []
        self.metadata: Dict[str, str] = {}
        self.allowed_base_urls = allowed_base_urls
        self.allowed_editions = allowed_editions
        self._check_edition(allowed_editions)
        self._check_instance(allowed_base_urls)
        self.supported_images_extensions = supported_images_extensions
        self.articles: Optional[List[Article]] = []
        logger.debug(f"Creating corpus `{self.id}`")

    def _check_edition(self, allowed_editions: List[str]) -> None:
        if self.edition not in allowed_editions:
            self.errors.append("Stylo edition not supported.")

    def _check_instance(self, allowed_base_urls: List[str]) -> None:
        if self.domain not in [get_domain_from_url(url) for url in allowed_base_urls]:
            self.errors.append("Stylo instance not supported.")

    @bail_on_errors
    def get_stylo_data(self) -> Optional[dict]:
        urls = dict((get_domain_from_url(url), url) for url in self.allowed_base_urls)
        url = urls[self.domain]
        styloapi = StyloAPI(url)

        try:
            response = styloapi.corpus(self.id)
        except httpx.HTTPStatusError as e:
            self.errors.append(e.args[0])
            return None

        data: dict = response.json()
        logger.debug(f"Fetched corpus with data `{data}`")

        if "errors" in data:
            for error in data["errors"]:
                self.errors.append(error["message"])
            logger.debug(f"Fetched corpus with errors `{data['errors']}`")
            return None

        corpus_data: dict = data["data"]["corpus"][0]

        self.name = self.name or corpus_data["name"]
        self.articles = [
            Article(
                self.edition,
                self.edition_configuration,
                self.domain,
                art["article"]["_id"],
                slugify(art["article"]["title"]),
                art["article"]["title"],
                self.root_download_dir,
                self.allowed_editions,
                self.allowed_base_urls,
                self.supported_images_extensions,
            )
            for art in corpus_data["articles"]
        ]
        return corpus_data

    def fetch_all(self, templates_folder: Path, force: bool = False) -> None:
        logger.debug(f"Fetching corpus `{self.id}`")
        self.metadata = self.get_stylo_data()
        logger.debug(f"Corpus data `{self.metadata}`")
        if self.metadata is not None and self.articles:
            for article in self.articles:
                article.fetch_all(templates_folder, force=force)

    def export(
        self,
        formats: List["str"],
        style_name: str,
        templates_folder: Path,
        with_toc: bool = False,
        with_ascii: bool = False,
        with_link_citations: bool = False,
        with_nocite: bool = False,
    ) -> Path:
        zip_file_path = self.download_dir / (
            f"{self.slug}-{self.id}-{'-'.join(sorted(formats))}-"
            f"toc-{int(with_toc)}-ascii-{int(with_ascii)}-"
            f"link-citations-{int(with_link_citations)}-"
            f"nocite-{int(with_nocite)}.zip"
        )
        # Ensure there is no on-disk cache for the generated zip file.
        zip_file_path.unlink(missing_ok=True)

        try:
            self.generate_archive(
                zip_file_path,
                formats,
                style_name,
                templates_folder,
                with_toc,
                with_ascii,
                with_link_citations,
                with_nocite,
            )
        except httpx.HTTPStatusError as exc:
            error = f"Erreur pendant la génération de « {self.name} »"
            if hasattr(exc, "url"):
                error += f" ({exc.url})"
            if hasattr(exc, "detail"):
                error += f" : {exc.detail}"
            self.errors.append(error)

        return zip_file_path

    def _write_to_archive(
        self, archive: ZipFile, file_name: Path, relative_to: Optional[Path] = None
    ) -> None:
        """A wrapper to generate correct relative paths within the archive."""
        arcname = file_name.relative_to(relative_to or self.download_dir.parent)
        if str(arcname) not in archive.namelist():
            archive.write(file_name, arcname=arcname)

    def add_metadata(self, zip_file_path: Path) -> None:
        yaml_file_path = self.download_dir.parent / "corpus.yaml"
        yaml_content = yaml.safe_dump_all(
            [{}, self.metadata, {}], default_flow_style=False
        )
        # We remove the added empty dicts necessary to retrieve the same structure
        # (one YAML document within a multiple one structure ---, dunno why).
        yaml_content = yaml_content[3:-3]
        yaml_file_path.write_text(yaml_content)
        with ZipFile(zip_file_path, mode="a") as archive:
            self._write_to_archive(archive, yaml_file_path)

    def generate_archive(
        self,
        zip_file_path: Path,
        formats: List["str"],
        style_name: str,
        templates_folder: Path,
        with_toc: bool,
        with_ascii: bool,
        with_link_citations: bool,
        with_nocite: bool,
    ) -> None:
        if self.metadata is not None and self.articles:
            for article in self.articles:
                article.generate_archive(
                    zip_file_path,
                    formats,
                    style_name,
                    templates_folder,
                    with_toc,
                    with_ascii,
                    with_link_citations,
                    with_nocite,
                )

            logger.debug(
                f"Archive generated for corpus `{self.id}` with formats `{formats}`"
            )

            self.add_metadata(zip_file_path)
