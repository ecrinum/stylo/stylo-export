from http import HTTPStatus
from pathlib import Path

import httpx
import pytest

from styloexport.utils import get_env_var

SE_PANDOC_API_BASE_URL = get_env_var("SE_PANDOC_API_BASE_URL")


def test_corpus_edition_generic_submission(client, context):
    response = client.post(
        "/generique/",
        data={
            "name": "Titre corpus",
            "kind": "corpus",
            "domain": "stylo.huma-num.fr",
            "article_id": "673777c4b15a620011acd308",
        },
    )
    assert response.status_code == HTTPStatus.FOUND
    assert response.headers["Location"] == (
        "/generique/corpus/stylo.huma-num.fr/673777c4b15a620011acd308/Titre-corpus/"
        "?with_toc=0&with_ascii=0&with_link_citations=0&with_nocite=0"
    )


def test_corpus_edition_submission_with_toc(client, context):
    response = client.post(
        "/generique/",
        data={
            "name": "Titre corpus",
            "kind": "corpus",
            "domain": "stylo.huma-num.fr",
            "article_id": "673777c4b15a620011acd308",
            "with_toc": True,
        },
    )
    assert response.status_code == HTTPStatus.FOUND
    assert response.headers["Location"] == (
        "/generique/corpus/stylo.huma-num.fr/673777c4b15a620011acd308/Titre-corpus/"
        "?with_toc=1&with_ascii=0&with_link_citations=0&with_nocite=0"
    )


def test_corpus_edition_submission_without_data(client, context):
    response = client.post("/generique/", data={})
    assert response.status_code == HTTPStatus.OK
    assert context[0]["form"].errors == {
        "name": ["Veuillez saisir un nom."],
        "article_id": ["Il est nécessaire de soumettre un ID."],
        "domain": ["Il est nécessaire de choisir le domaine."],
        "kind": ["Il est nécessaire de choisir un type."],
    }
    assert "Nom de votre export" in response.get_data(as_text=True)
    assert "ID de votre article" in response.get_data(as_text=True)
    assert "Récupérer" in response.get_data(as_text=True)


def test_corpus_generique(client, context, httpx_mock, tmp_path):
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "Corpus.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.graphql").read_text(),
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/html/"
            "?name=bibliography.html&with_toc=false&with_ascii=false&standalone=false"
        ),
        status_code=500,
    )
    httpx_mock.add_response(
        url="https://avatars2.githubusercontent.com/u/16691667?s=200&v=4",
        content=(
            Path() / "tests" / "fixtures" / "7dc926ed4014a96a908e5fb21de52329"
        ).read_bytes(),
        headers={"content-type": "image/png"},
    )
    response = client.get(
        "/generique/corpus/stylo.huma-num.fr/673777c4b15a620011acd308/test/"
    )
    assert response.status_code == HTTPStatus.OK
    assert context[0]["corpus"].domain == "stylo.huma-num.fr"
    assert context[0]["corpus"].slug == "test"
    assert context[0]["corpus"].id == "673777c4b15a620011acd308"
    assert len(context[0]["form"].formats.choices) == 11
    assert len(context[0]["form"].bibliography_style.choices) == 2
    assert len(context[0]["form"].bibliography_style.choices["Styles Zotero"]) == 199
    assert context[0]["form"].version.choices == [("", "Version la plus récente")]
    assert (
        context[0]["bibliography_preview"]
        == "La génération de la prévisualisation a échoué."
    )
    assert context[0]["corpus"].errors == []
    assert str(next(iter(tmp_path.iterdir()))).endswith("generique")
    generated_files = sorted(
        list(
            (
                tmp_path
                / "generique"
                / "stylo-huma-num-fr"
                / "corpus"
                / "test-673777c4b15a620011acd308"
            ).iterdir()
        )
    )
    assert len(generated_files) == 0

    assert (
        len(
            list(
                (
                    tmp_path
                    / "generique"
                    / "stylo-huma-num-fr"
                    / "articles"
                    / "how-to-stylo-5e32d54e4f58270018f9d251"
                    / "originals"
                ).iterdir()
            )
        )
        == 3
    )
    assert (
        len(
            list(
                (
                    tmp_path
                    / "generique"
                    / "stylo-huma-num-fr"
                    / "articles"
                    / "how-to-stylo-5e32d54e4f58270018f9d251"
                    / "images"
                ).iterdir()
            )
        )
        == 1
    )


def test_corpus_with_404(client, context, httpx_mock):
    httpx_mock.add_response(url="https://stylo.huma-num.fr/graphql", status_code=404)
    response = client.get(
        "/generique/corpus/stylo.huma-num.fr/673777c4b15a620011acd308/test/"
    )
    assert response.status_code == HTTPStatus.OK
    assert context[0]["corpus"].errors == [
        """
Client error '404 Not Found' for url 'https://stylo.huma-num.fr/graphql'
For more information check: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/404
    """.strip()
    ]


def test_corpus_with_503_from_stylo(client, context, httpx_mock):
    httpx_mock.add_exception(httpx.RequestError("Unable to access Stylo API."))
    with pytest.raises(httpx.RequestError):
        client.get("/generique/corpus/stylo.huma-num.fr/673777c4b15a620011acd308/test/")


def test_corpus_downloads(client, tmp_path):
    (
        tmp_path
        / "generique"
        / "stylo.huma-num.fr"
        / "corpus"
        / "test-673777c4b15a620011acd308"
    ).mkdir(parents=True)
    (
        tmp_path
        / "generique"
        / "stylo.huma-num.fr"
        / "corpus"
        / "test-673777c4b15a620011acd308"
        / "test-673777c4b15a620011acd308.md"
    ).write_text("content")
    response = client.get(
        (
            "/generique/corpus/downloads/stylo.huma-num.fr"
            "/673777c4b15a620011acd308/test/test-673777c4b15a620011acd308.md"
        )
    )
    assert response.status_code == HTTPStatus.OK
    assert response.content_length == len("content")


def test_corpus_downloads_inexisting_file(client, tmp_path):
    response = client.get(
        (
            "/generique/corpus/downloads/stylo.huma-num.fr"
            "/673777c4b15a620011acd308/test/test-673777c4b15a620011acd308.md"
        )
    )
    assert response.status_code == HTTPStatus.NOT_FOUND
