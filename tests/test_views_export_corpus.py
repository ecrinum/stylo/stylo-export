from http import HTTPStatus
from pathlib import Path

from styloexport.utils import get_env_var

SE_PANDOC_API_BASE_URL = get_env_var("SE_PANDOC_API_BASE_URL")


def test_export_generique_all(app, client, context, httpx_mock, tmp_path):
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "Corpus.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://avatars2.githubusercontent.com/u/16691667?s=200&v=4",
        content=(
            Path() / "tests" / "fixtures" / "7dc926ed4014a96a908e5fb21de52329"
        ).read_bytes(),
        headers={"content-type": "image/png"},
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/html/"
            "?name=how-to-stylo.html&with_toc=false&with_ascii=false&with_link_citations=false"
            "&with_nocite=false"
        ),
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.html").read_bytes(),
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/tex/?name=how-to-stylo.tex&with_toc=false"
            "&with_link_citations=false&with_nocite=false"
        ),
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.tex").read_bytes(),
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/xml/tei/?name=how-to-stylo-tei.xml"
            "&with_link_citations=false&with_nocite=false"
        ),
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo-tei.xml").read_bytes(),
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/xml/erudit/?name=how-to-stylo-erudit.xml"
            "&with_link_citations=false&with_nocite=false"
        ),
        content=(
            Path() / "tests" / "fixtures" / "How_to_Stylo-erudit.xml"
        ).read_bytes(),
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/pdf/?name=how-to-stylo.pdf&with_toc=false"
            "&with_link_citations=false&with_nocite=false"
        ),
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.pdf").read_bytes(),
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/odt/?name=how-to-stylo.odt&with_toc=false"
            "&with_link_citations=false&with_nocite=false"
        ),
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.odt").read_bytes(),
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/docx/?name=how-to-stylo.docx&with_toc=false"
            "&with_link_citations=false&with_nocite=false"
        ),
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.docx").read_bytes(),
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/icml/?name=how-to-stylo.icml"
            "&with_link_citations=false&with_nocite=false"
        ),
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.icml").read_bytes(),
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/xml/tei/?name=how-to-stylo-metopes-tei.xml"
            "&with_link_citations=false&with_nocite=false"
        ),
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo-tei.xml").read_bytes(),
    )

    formats = (
        "formats=originals&formats=md-ssg&formats=html&formats=tex&formats=pdf"
        "&formats=odt&formats=docx"
    )
    formats_xml = (
        "formats=icml&formats=xml-tei&formats=xml-erudit&formats=xml-tei-metopes"
    )
    response = client.get(
        (
            "/generique/corpus/export/stylo.huma-num.fr/673777c4b15a620011acd308/test/"
            f"?{formats}&{formats_xml}&bibliography_style=chicagomodified&version="
        )
    )

    assert response.status_code == HTTPStatus.OK
    formats = "docx-html-icml-md-ssg-odt-originals-pdf-tex"
    formats_xml = "xml-erudit-xml-tei-xml-tei-metopes"
    filename = (
        f"test-673777c4b15a620011acd308-{formats}-{formats_xml}-"
        "toc-0-ascii-0-link-citations-0-nocite-0.zip"
    )
    assert response.headers["Content-Disposition"] == f"attachment; filename={filename}"


def test_export_generique_all_with_error_from_pandoc(
    app, client, context, httpx_mock, tmp_path
):
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "Corpus.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://avatars2.githubusercontent.com/u/16691667?s=200&v=4",
        content=(
            Path() / "tests" / "fixtures" / "7dc926ed4014a96a908e5fb21de52329"
        ).read_bytes(),
        headers={"content-type": "image/png"},
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/html/"
            "?name=how-to-stylo.html&with_toc=false&with_ascii=false"
            "&with_link_citations=false&with_nocite=false"
        ),
        status_code=422,
        json={
            "detail": [
                {
                    "ctx": {"limit_value": 8},
                    "loc": ["query", "name"],
                    "msg": "ensure this value has at least 8 characters",
                    "type": "value_error.any_str.min_length",
                }
            ]
        },
    )
    formats = (
        "formats=originals&formats=html&formats=tex&formats=pdf&formats=odt"
        "&formats=docx"
    )
    formats_xml = (
        "formats=icml&formats=xml-tei&formats=xml-erudit&formats=xml-tei-metopes"
    )
    response = client.get(
        (
            "/generique/corpus/export/stylo.huma-num.fr/673777c4b15a620011acd308/a/"
            f"?{formats}&{formats_xml}&bibliography_style=chicagomodified&version="
        )
    )

    assert response.status_code == HTTPStatus.OK
    assert context[0]["corpus"].errors == [
        (
            "Erreur pendant la génération de «\xa0corpus-export-test\xa0» "
            "(convert/html/)\xa0: "
            "[{'ctx': {'limit_value': 8}, 'loc': ['query', 'name'], "
            "'msg': 'ensure this value has at least 8 characters', "
            "'type': 'value_error.any_str.min_length'}]"
        )
    ]


def test_export_generique_html_only(app, client, context, httpx_mock, tmp_path):
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "Corpus.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://avatars2.githubusercontent.com/u/16691667?s=200&v=4",
        content=(
            Path() / "tests" / "fixtures" / "7dc926ed4014a96a908e5fb21de52329"
        ).read_bytes(),
        headers={"content-type": "image/png"},
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/html/"
            "?name=how-to-stylo.html&with_toc=false&with_ascii=false"
            "&with_link_citations=false&with_nocite=false"
        ),
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.html").read_bytes(),
    )

    formats = "formats=html"
    response = client.get(
        (
            f"/generique/corpus/export/stylo.huma-num.fr/673777c4b15a620011acd308/test/"
            f"?{formats}&bibliography_style=chicagomodified&version="
        )
    )

    assert response.status_code == HTTPStatus.OK
    filename = (
        "test-673777c4b15a620011acd308-html-toc-0-ascii-0-link-citations-0-nocite-0.zip"
    )
    assert response.headers["Content-Disposition"] == f"attachment; filename={filename}"


def test_export_generique_md_ssg_only(app, client, context, httpx_mock, tmp_path):
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "Corpus.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://avatars2.githubusercontent.com/u/16691667?s=200&v=4",
        content=(
            Path() / "tests" / "fixtures" / "7dc926ed4014a96a908e5fb21de52329"
        ).read_bytes(),
        headers={"content-type": "image/png"},
    )

    formats = "formats=md-ssg"
    response = client.get(
        (
            f"/generique/corpus/export/stylo.huma-num.fr/673777c4b15a620011acd308/test/"
            f"?{formats}&bibliography_style=chicagomodified&version="
        )
    )

    assert response.status_code == HTTPStatus.OK
    filename = (
        "test-673777c4b15a620011acd308-md-ssg-"
        "toc-0-ascii-0-link-citations-0-nocite-0.zip"
    )
    assert response.headers["Content-Disposition"] == f"attachment; filename={filename}"


def test_export_unsupported_format(client, context, httpx_mock, tmp_path):
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "Corpus.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://avatars2.githubusercontent.com/u/16691667?s=200&v=4",
        content=(
            Path() / "tests" / "fixtures" / "7dc926ed4014a96a908e5fb21de52329"
        ).read_bytes(),
    )

    response = client.get(
        (
            "/sens-public/corpus/export/stylo.huma-num.fr/673777c4b15a620011acd308/test/"
            "?formats=unsupported_format&version="
        )
    )

    assert response.status_code == HTTPStatus.OK
    assert context[0]["form"].errors == {"formats": ["Not a valid choice."]}
