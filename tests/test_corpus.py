import json
from pathlib import Path
from zipfile import ZipFile

from styloexport.config import SE_EDITIONS
from styloexport.corpus import Corpus
from styloexport.utils import get_env_var

SE_PANDOC_API_BASE_URL = get_env_var("SE_PANDOC_API_BASE_URL")


def test_corpus_generique(httpx_mock, tmp_path):
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "Corpus.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://avatars2.githubusercontent.com/u/16691667?s=200&v=4",
        content=(
            Path() / "tests" / "fixtures" / "7dc926ed4014a96a908e5fb21de52329"
        ).read_bytes(),
        headers={"content-type": "image/png"},
    )

    corpus = Corpus(
        "generique",
        SE_EDITIONS["generique"],
        "stylo.huma-num.fr",
        "673777c4b15a620011acd308",
        "test",
        "",
        tmp_path,
        ["generique"],
        ["https://stylo.huma-num.fr"],
        [],
    )
    templates_folder = (Path() / "templates" / "generique").resolve()
    corpus.fetch_all(templates_folder)

    assert corpus.edition == "generique"
    assert corpus.domain == "stylo.huma-num.fr"
    assert corpus.slug == "test"
    assert corpus.id == "673777c4b15a620011acd308"
    assert corpus.errors == []
    assert str(next(iter(tmp_path.iterdir()))).endswith("generique")
    generated_files = sorted(
        list(
            (
                tmp_path
                / "generique"
                / "stylo-huma-num-fr"
                / "corpus"
                / "test-673777c4b15a620011acd308"
            ).iterdir()
        )
    )
    assert len(generated_files) == 0

    generated_files = sorted(
        list(
            (
                tmp_path
                / "generique"
                / "stylo-huma-num-fr"
                / "articles"
                / "how-to-stylo-5e32d54e4f58270018f9d251"
            ).iterdir()
        )
    )
    assert str(generated_files[0]).endswith("images")
    assert str(generated_files[1]).endswith("images.zip")
    assert str(generated_files[2]).endswith("originals")
    assert (
        len(
            list(
                (
                    tmp_path
                    / "generique"
                    / "stylo-huma-num-fr"
                    / "articles"
                    / "how-to-stylo-5e32d54e4f58270018f9d251"
                    / "originals"
                ).iterdir()
            )
        )
        == 3  # md + bib + yaml.
    )


def test_corpus_generique_only_html(app, httpx_mock, tmp_path, mocker):
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "Corpus.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://avatars2.githubusercontent.com/u/16691667?s=200&v=4",
        content=(
            Path() / "tests" / "fixtures" / "7dc926ed4014a96a908e5fb21de52329"
        ).read_bytes(),
        headers={"content-type": "image/png"},
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/html/"
            "?name=how-to-stylo.html&with_toc=false&with_ascii=false"
            "&with_link_citations=false&with_nocite=false"
        ),
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.html").read_bytes(),
    )
    zipfile_write = mocker.patch("styloexport.article.ZipFile.write")
    zipfile_write.return_value = "dummy"

    corpus = Corpus(
        "generique",
        SE_EDITIONS["generique"],
        "stylo.huma-num.fr",
        "673777c4b15a620011acd308",
        "test",
        "",
        tmp_path,
        ["generique"],
        ["https://stylo.huma-num.fr"],
        [".png"],
    )
    templates_folder = (Path() / "templates" / "generique").resolve()
    corpus.fetch_all(templates_folder)
    export_file_path = corpus.export(
        ["originals", "html"], "chicagomodified", templates_folder
    )

    assert export_file_path.exists()
    assert (
        zipfile_write.call_count == 7
    )  # md + bib + yaml + images + html + corpus yaml
    assert str(next(iter(tmp_path.iterdir()))).endswith("generique")
    generated_files = sorted(
        list(
            (
                tmp_path
                / "generique"
                / "stylo-huma-num-fr"
                / "corpus"
                / "test-673777c4b15a620011acd308"
            ).iterdir()
        )
    )
    assert len(generated_files) == 1


def test_corpus_generique_only_md_ssg(app, httpx_mock, tmp_path):
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "Corpus.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://avatars2.githubusercontent.com/u/16691667?s=200&v=4",
        content=(
            Path() / "tests" / "fixtures" / "7dc926ed4014a96a908e5fb21de52329"
        ).read_bytes(),
        headers={"content-type": "image/png"},
    )

    corpus = Corpus(
        "generique",
        SE_EDITIONS["generique"],
        "stylo.huma-num.fr",
        "673777c4b15a620011acd308",
        "test",
        "",
        tmp_path,
        ["generique"],
        ["https://stylo.huma-num.fr"],
        [".png"],
    )
    templates_folder = (Path() / "templates" / "generique").resolve()
    corpus.fetch_all(templates_folder)
    export_file_path = corpus.export(["md-ssg"], "chicagomodified", templates_folder)

    assert export_file_path.exists()
    # assert zipfile_write.call_count == 3  # md + images * 2
    assert str(next(iter(tmp_path.iterdir()))).endswith("generique")
    generated_files = sorted(
        list(
            (
                tmp_path
                / "generique"
                / "stylo-huma-num-fr"
                / "corpus"
                / "test-673777c4b15a620011acd308"
            ).iterdir()
        )
    )
    assert len(generated_files) == 1
    assert str(generated_files[0]).endswith(
        "md-ssg-toc-0-ascii-0-link-citations-0-nocite-0.zip"
    )
    assert ZipFile(generated_files[0]).namelist() == [
        (
            "how-to-stylo-5e32d54e4f58270018f9d251/images/"
            "how-to-stylo-7dc926ed4014a96a908e5fb21de52329.png"
        ),
        "how-to-stylo.md",
        "corpus.yaml",
    ]


def test_corpus_generique_export_with_pdf_pandoc_error(
    app, httpx_mock, tmp_path, mocker
):
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "Corpus.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://stylo.huma-num.fr/graphql",
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.graphql").read_text(),
    )
    httpx_mock.add_response(
        url="https://avatars2.githubusercontent.com/u/16691667?s=200&v=4",
        content=(
            Path() / "tests" / "fixtures" / "7dc926ed4014a96a908e5fb21de52329"
        ).read_bytes(),
        headers={"content-type": "image/png"},
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/html/"
            "?name=how-to-stylo.html&with_toc=false&with_ascii=false"
            "&with_link_citations=false&with_nocite=false"
        ),
        content=(Path() / "tests" / "fixtures" / "How_to_Stylo.html").read_bytes(),
    )
    httpx_mock.add_response(
        url=(
            f"{SE_PANDOC_API_BASE_URL}convert/pdf/?name=how-to-stylo.pdf&with_toc=false"
            "&with_link_citations=false&with_nocite=false"
        ),
        status_code=500,
        content=json.dumps({"detail": "Pandoc error"}),
    )
    zipfile_write = mocker.patch("styloexport.article.ZipFile.write")
    zipfile_write.return_value = "dummy"

    corpus = Corpus(
        "generique",
        SE_EDITIONS["generique"],
        "stylo.huma-num.fr",
        "673777c4b15a620011acd308",
        "test",
        "",
        tmp_path,
        ["generique"],
        ["https://stylo.huma-num.fr"],
        [".png"],
    )
    templates_folder = (Path() / "templates" / "generique").resolve()
    corpus.fetch_all(templates_folder)
    export_file_path = corpus.export(
        ["originals", "html", "pdf"], "chicagomodified", templates_folder
    )

    assert export_file_path.exists()
    # image archive + md + bib + yaml + html + pdf
    assert zipfile_write.call_count == 6
    assert corpus.errors == [
        (
            "Erreur pendant la génération de « corpus-export-test » "
            "(convert/pdf/) : Pandoc error"
        )
    ]
